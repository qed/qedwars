function getJSONFromURL(url) {
  var request = new XMLHttpRequest();
  request.open('GET', url, false);
  request.send(null);
  return JSON.parse(request.responseText);
}

function removeAll(cls) {
  var elements = document.getElementsByClassName(cls);
  while (elements.length > 0) {
    elements[0].remove();
  }
}

function addRow(table, cls, rk, pts, war, ply, ts, id) {
  var row = document.createElement('tr');
  var rankColumn = document.createElement('td');
  var pointsColumn = document.createElement('td');
  var warriorColumn = document.createElement('td');
  var playerColumn = document.createElement('td');
  var timeColumn = document.createElement('td');
  var idColumn = document.createElement('td');
  var st = 'border:1px solid black; padding: 3px';
  row.className = cls;
  rankColumn.setAttribute('style', st);
  pointsColumn.setAttribute('style', st);
  warriorColumn.setAttribute('style', st);
  playerColumn.setAttribute('style', st);
  timeColumn.setAttribute('style', st);
  idColumn.setAttribute('style', st);
  rankColumn.innerHTML = rk;
  pointsColumn.innerHTML = pts;
  warriorColumn.innerHTML = war;
  playerColumn.innerHTML = ply;
  timeColumn.innerHTML = ts;
  idColumn.innerHTML = '<a href="#results" ' +
                       'style="font-family: monospace; font-size: large" ' +
                       'onclick="document.getElementById(\'id\').value = \'' +
                       id + '\'; updateResults()">' + id + '</a>';
  row.appendChild(rankColumn);
  row.appendChild(pointsColumn);
  row.appendChild(warriorColumn);
  row.appendChild(playerColumn);
  row.appendChild(timeColumn);
  row.appendChild(idColumn);
  table.appendChild(row);
}

Array.prototype.swap = function (i, j) {
  var temp = this[i];
  this[i] = this[j];
  this[j] = temp;
  return this;
}

function sortScores(scores) {
  for (var i = 0; i < scores.length - 1; i++) {
    for (var j = i + 1; j < scores.length; j++) {
      if (scores[j]['score'] > scores[i]['score']) {
        scores.swap(i, j);
      }
    }
  }
  return scores;
}

function bestOfN(results) {
  var scores = [];
  for (var warriorID in results) {
    if (results.hasOwnProperty(warriorID)) {
      var score = 0;
      for (var opponentID in results[warriorID]['matches']) {
        if (results[warriorID]['matches'].hasOwnProperty(opponentID)) {
         if (results[warriorID]['matches'][opponentID]['wins'] >
             results[warriorID]['matches'][opponentID]['losses']) {
           score += 1;
         }
         else if (results[warriorID]['matches'][opponentID]['losses'] >
                  results[warriorID]['matches'][opponentID]['wins']) {
           score -= 1;
         }
        }
      }
      scores.push({'id' : warriorID, 'score' : score});
    }
  }
  return sortScores(scores);
}

function threePointsForAWin(results) {
  var scores = [];
  for (var warriorID in results) {
    if (results.hasOwnProperty(warriorID)) {
      var score = 0;
      for (var opponentID in results[warriorID]['matches']) {
        if (results[warriorID]['matches'].hasOwnProperty(opponentID)) {
          score += 3 * results[warriorID]['matches'][opponentID]['wins'] +
                   results[warriorID]['matches'][opponentID]['ties'];
        }
      }
      scores.push({'id' : warriorID, 'score' : score});
    }
  }
  return sortScores(scores);
}

function customRule(results) {
  eval(document.getElementById('custom-ranking-code').value);
  return myRule(results)
}

function updateScores() {
  var results = getJSONFromURL('results.json');
  var board = document.getElementById('scoreboard');
  var radioButtons = document.getElementsByName('ranking-rule')
  var scoreboard, rankingFunction;
  for(var i = 0; i < radioButtons.length; i++){
    if(radioButtons[i].checked){
        rankingFunction = eval(radioButtons[i].value);
    }
  }
  scoreboard = rankingFunction(results);
  removeAll('scoreboard-row');
  for (var i = 0; i < scoreboard.length; i++) {
    addRow(board, 'scoreboard-row',
           '<b>#' + (i + 1).toString() + '</b>',
           scoreboard[i]['score'].toString(),
           results[scoreboard[i]['id']]['name'],
           results[scoreboard[i]['id']]['author'],
           new Date(results[scoreboard[i]['id']]['timestamp'] * 1000),
           scoreboard[i]['id']);
  }
  board.style.display = '';
}

function updateResults() {
  var table = document.getElementById('results');
  var results = getJSONFromURL('results.json');
  var warriorID = document.getElementById('id').value;
  removeAll('result-row');
  if (results.hasOwnProperty(warriorID)) {
    var matches = results[warriorID]['matches'];
    for (var opponentID in results) {
      if (results.hasOwnProperty(opponentID)) {
        var winsString, lossesString;
        if (warriorID == opponentID) {
          winsString = 'N/A';
          lossesString = 'N/A';
        }
        else {
          winsString = matches[opponentID]['wins'].toString();
          lossesString = matches[opponentID]['losses'].toString();
        }
        addRow(table,
               'result-row',
               winsString,
               lossesString,
               results[opponentID]['name'],
               results[opponentID]['author'],
               new Date(results[opponentID]['timestamp'] * 1000),
               opponentID);
      }
    }
    table.style.display = '';
  }
}
