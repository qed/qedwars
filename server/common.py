import json
import time

log_path = 'qedwars.log'

def log(string):
  log_file = open(log_path, 'a')
  log_file.write(time.strftime('%Y-%m-%d %H:%M:%S %Z - ',
                               time.localtime(time.time())) +
                 string +
                '\n')
  log_file.close()

def read_json(path):
  f = open(path, 'r')
  players = json.load(f)
  f.close()
  return players

def write_json(content, path):
  f = open(path, 'w')
  json.dump(content, f, indent=2)
  f.close()
