import BaseHTTPServer, SocketServer
import bcrypt
import cgi
import hashlib
import os
import time
import thread, threading

from common import log, read_json, write_json

default_index = 'index-qed.html'

def add_warrior(code, secret):
  try:
    if code != '':
      timestamp = str(time.time())
      warrior_id = hashlib.sha1('; ' + timestamp + '\n' +
                                '; ' + secret + '\n' +
                                code).hexdigest()
      author = '[unknown]'
      name = '[unknown]'
      for line in code.splitlines():
        phrases = line.split()
        if len(phrases) > 0 and phrases[0] == ';author':
          author = ' '.join(phrases[1:])
        elif len(phrases) > 1 and phrases[0] == ';' and phrases[1] == 'author':
          author = ' '.join(phrases[2:])
        elif len(phrases) > 0 and phrases[0] == ';name':
          name = ' '.join(phrases[1:])
        elif len(phrases) > 1 and phrases[0] == ';' and phrases[1] == 'name':
          name = ' '.join(phrases[2:])
      players = read_json('../data/players.json')
      players[warrior_id] = {
        'author' : cgi.escape(author, True),
        'name' : cgi.escape(name, True),
        'timestamp' : timestamp,
        'hash' : bcrypt.hashpw(secret, bcrypt.gensalt())
      }
      write_json(players, '../data/players.json')
      f = open('../data/' + warrior_id + '.red', 'w')
      f.write(code)
      f.close()
      log('added id ' + warrior_id)
      return warrior_id
    else:
      return None
  except:
    return None

def remove_warrior(warrior_id, secret):
  try:
    players = read_json('../data/players.json')
    if players.has_key(warrior_id):
      if bcrypt.checkpw(secret, players[warrior_id]['hash'].encode()):
        results = read_json('../public/results.json')
        if results.has_key(warrior_id):
          del results[warrior_id]
          for value in results.itervalues():
            if value['matches'].has_key(warrior_id):
              del value['matches'][warrior_id]
          del players[warrior_id]
          write_json(players, '../data/players.json')
          write_json(results, '../public/results.json')
          os.remove('../data/' + warrior_id + '.red')
          return {'success' : True, 'message': 'REMOVED ID ' + warrior_id}
      else:
        return {'success' : False, 'message' : 'WRONG SECRET'}
    else:
      return {'success' : False, 'message' : 'ID NOT FOUND'}
  except:
    return {'success' : False,
            'message' : 'AN ERROR OCCURED DURING REMOVAL ATTEMPT'}

class ThreadedHTTPServer(SocketServer.ThreadingMixIn, BaseHTTPServer.HTTPServer):
  pass

class IncomingScoreHandler(BaseHTTPServer.BaseHTTPRequestHandler):
  server_version = 'QEDWars/0.5'

  def do_GET(self):
    try:
      if self.path == '/':
        self.path = '/index-qed.html'
      self.send_response(200)
      content_type = 'text/plain'
      extension = self.path.split('.')[-1]
      if extension == 'html':
        content_type = 'text/html'
      elif extension == 'js':
        content_tye = 'text/javascript'
      elif extension == 'json':
        content_tye = 'application/json'
      self.send_header('Content-type', content_type)
      self.end_headers()
      f = open('../public' + self.path, 'r')
      self.wfile.write(f.read())
      f.close()
    except:
      self.send_response(404)
      self.end_headers()
      self.wfile.write('PAGE NOT FOUND')

  def do_POST(self):
    try:
      query = self.rfile.read(int(self.headers['Content-Length']))
      vals = cgi.parse_qs(query)
      status_code = 406
      response_text = 'INVALID QUERY'
      actions = vals.get('action', [''])
      if actions is not None:
        if actions[0]  == 'add':
          warrior_id = add_warrior(vals.get('code', [''])[0],
                                   vals.get('secret', [''])[0])
          if warrior_id is not None:
            status_code = 200
            response_text = 'ID: ' + warrior_id
          else:
            response_text = 'FAILED TO ADD WARRIOR'
        elif actions[0]  == 'remove':
          secret = ''
          if vals.has_key('secret'):
            secret = vals.get('secret')[0]
          removal_status = remove_warrior(vals.get('id')[0], secret)
          if removal_status['success']:
            status_code = 200
          response_text = removal_status['message']
      self.send_response(status_code)
      self.send_header('Content-type', 'text/html')
      self.end_headers()
      self.wfile.write('<html><head></head><body>' +
                       response_text +
                       '</body></html>')
      log('written response: ' + response_text)
    except:
      log('error while handling request')

if __name__ == '__main__':
  try:
    if not os.path.isfile('../data/players.json'):
      log('couldn\'t find player list, creating it')
      write_json({}, '../data/players.json')
    server = ThreadedHTTPServer(('', 55555), IncomingScoreHandler)
    server_thread = threading.Thread(target = server.serve_forever)
    server_thread.start()
    log('server started')
  except:
    log('quitting, initialization may have been unsuccessful')

