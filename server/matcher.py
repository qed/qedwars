import os
import subprocess
import time

from common import log, read_json, write_json

if __name__ == '__main__':
  if not os.path.isfile('../public/results.json'):
      log('couldn\'t find results file, creating it')
      write_json({}, '../public/results.json')
  while True:
    players = read_json('../data/players.json')
    results = read_json('../public/results.json')
    for warrior_id, data in players.iteritems():
      if not results.has_key(warrior_id):
        log('running matches for id ' + warrior_id)
        results[warrior_id] = {
          'author' : data['author'],
          'name' : data['name'],
          'timestamp' : data['timestamp'],
          'matches' : {}
        }
        for opponent_id in results.keys():
          if opponent_id != warrior_id:
            command = '../data/pmars -b -r 100 '
            command += '../data/' + warrior_id + '.red '
            command += '../data/' + opponent_id + '.red '
            command += '2>&1'
            process = subprocess.Popen(command,
                                       shell = True,
                                       stdout = subprocess.PIPE)
            output = process.stdout.read()
            result = output.split()
            ties, warrior_wins, opponent_wins = -1, -1, -1
            try:
              ties = int(result[-1])
              opponent_wins = int(result[-2])
              warrior_wins = int(result[-3])
            except:
              log('problem in match ' + warrior_id + ' vs. ' + opponent_id)
              log(output)
            results[warrior_id]['matches'][opponent_id] = {
              'wins' : warrior_wins,
              'losses' : opponent_wins,
              'ties' : ties
            }
            results[opponent_id]['matches'][warrior_id] = {
              'wins' : opponent_wins,
              'losses' : warrior_wins,
              'ties' : ties
            }
        write_json(results, '../public/results.json')
        log('matches for id ' + warrior_id + ' completed')
    time.sleep(10)

